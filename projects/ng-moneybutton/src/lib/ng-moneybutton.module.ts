import { NgModule } from '@angular/core';
import { NgMoneybuttonComponent } from './ng-moneybutton.component';



@NgModule({
  declarations: [NgMoneybuttonComponent],
  imports: [
  ],
  exports: [NgMoneybuttonComponent]
})
export class NgMoneybuttonModule { }
