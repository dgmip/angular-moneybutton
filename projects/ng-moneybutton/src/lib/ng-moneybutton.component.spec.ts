import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgMoneybuttonComponent } from './ng-moneybutton.component';

describe('NgMoneybuttonComponent', () => {
  let component: NgMoneybuttonComponent;
  let fixture: ComponentFixture<NgMoneybuttonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgMoneybuttonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgMoneybuttonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
