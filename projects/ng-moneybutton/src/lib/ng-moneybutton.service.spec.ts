import { TestBed } from '@angular/core/testing';

import { NgMoneybuttonService } from './ng-moneybutton.service';

describe('NgMoneybuttonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgMoneybuttonService = TestBed.get(NgMoneybuttonService);
    expect(service).toBeTruthy();
  });
});
